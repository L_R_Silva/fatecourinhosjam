﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeInvisible : MonoBehaviour {

    private Ray ray;
    private RaycastHit hit;
    [SerializeField]
    private GameObject diver;
    private MeshRenderer OldRend;

    private void Start()
    {
            
    }

    void Update()
    {
        Vector3 _screenPoint = Camera.main.WorldToScreenPoint(diver.transform.position);
        ray = Camera.main.ScreenPointToRay(_screenPoint);
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.gameObject.tag == "Arco")
            {
                MeshRenderer _rend = hit.transform.GetComponent<MeshRenderer>();
                OldRend = _rend;
                _rend.material.shader = Shader.Find("Transparent/Diffuse");
                Color tempColor = _rend.material.color;
                tempColor.a = 0.25F;
                _rend.material.color = tempColor;
            }
            else if(OldRend != null)
            {
                Color tempColor = OldRend.material.color;
                tempColor.a = 1f;
                OldRend.material.color = tempColor;
                OldRend.material.shader = Shader.Find("Standard");
            }
        }

    }


}
