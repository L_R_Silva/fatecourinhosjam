﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesactivateOnCover : MonoBehaviour {

    private Ray ray;
    private RaycastHit hit;
    [SerializeField]
    private GameObject diver;
    void Update()
    {
        Vector3 _screenPoint = Camera.main.WorldToScreenPoint(diver.transform.position);
        ray = Camera.main.ScreenPointToRay(_screenPoint);
        if(Physics.Raycast(ray, out hit))
        {
            if (hit.transform.gameObject.tag != "Player")
            {
                DesactivateActivate();
            }
        }

    }

    private IEnumerator DesactivateActivate()
    {
        hit.transform.gameObject.SetActive(false);
        yield return new WaitForSeconds(.1f);
        hit.transform.gameObject.SetActive(true);
    }
}
