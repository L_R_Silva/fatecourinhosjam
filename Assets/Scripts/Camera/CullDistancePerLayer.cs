﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CullDistancePerLayer : MonoBehaviour {

    private Camera mainCamera;
    private float[] distances;

    private void Start()
    {
        mainCamera = GetComponent<Camera>();
        //32 layers no total, cada posição do vetor representa uma layer
        distances = new float[32];
        SetLayersCullDistance();
    }

    private void SetLayersCullDistance()
    {
        //Corais
        distances[8] = 85;
        //Rochas
        distances[9] = 95;
        mainCamera.layerCullDistances = distances;
    }
	
	
}
