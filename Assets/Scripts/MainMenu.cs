﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    [SerializeField]
    private Animator painel;
    [SerializeField]
    private Animator backGround;
    [SerializeField]
    private Animator fade;

    private void Start()
    {
        Time.timeScale = 1f;
    }

    public void ShowCredits()
    {
        painel.SetBool("CreditosSelected",true);
    }

    public void ShowHighScore()
    {
        painel.SetBool("HighScoreSelected", true);
    }

    public void Play()
    {
        painel.SetBool("PlaySelect", true);
        backGround.SetBool("Start", true);
        fade.SetBool("Fade",true);
        StartCoroutine("LoadYourAsyncScene");    
    }

    public void VoltarMenuInicial()
    {
        painel.SetBool("HighScoreSelected", false);
        painel.SetBool("CreditosSelected", false);
    }

    private IEnumerator LoadYourAsyncScene()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("TEST");
        // Wait until the asynchronous scene fully loads
        yield return new WaitForSeconds(1f);
        
        
    }
}
