﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMov : MonoBehaviour {

    [SerializeField]
    private Animator diver;
    [SerializeField]
    private Vector3 leftPos;
    [SerializeField]
    private Vector3 rightPos;
    private Vector3 upPos;

    private Vector3 startPos;
    private Vector3 finishPos;
    private Vector3 lerpMov;

    private float timeOfStart;
    private float timeToPoint = 0.15f;
    private float percOfCompletion;

    private bool isMoving = false;

    //Static Variables
    public static Vector3 staticRightPos;
    public static Vector3 staticLeftPos;

    private void Start()
    {
        staticRightPos = rightPos;
        staticLeftPos = leftPos;
    }
    private void Update()
    {
        if (isMoving == false)
        {
            StartLerp();
        }
    }

    private void FixedUpdate()
    {
        if (isMoving == true)
        { 
            float _timeSinceStart = Time.time - timeOfStart;
            percOfCompletion = _timeSinceStart / timeToPoint;
            if (diver.GetBool("turnUp") == true)
            {
                StartCoroutine("UpAndDown");
            }
            else
            {
                lerpMov = Vector3.Lerp(startPos, finishPos, percOfCompletion);
            }
            this.transform.position = lerpMov;
            
            if (percOfCompletion >= 1f)
            {
                if (diver.GetBool("turnLeft") == true)
                {
                    diver.SetBool("turnLeft", false);
                    isMoving = false;
                }

                if (diver.GetBool("turnRight") == true)
                {
                    diver.SetBool("turnRight", false);
                    isMoving = false;
                }

            }
        }

    }

    private void StartLerp()
    {
        startPos = this.transform.position;
        upPos = new Vector3(this.transform.position.x, 5f);

        if (diver.GetBool("turnLeft") == true && diver.GetBool("turnUp") == false)
        {
            if (startPos.x > leftPos.x)
            {
                isMoving = true;
                timeOfStart = Time.time;
                finishPos = new Vector3(startPos.x - 4.67f, leftPos.y,leftPos.z);
            }
            else
            {
                diver.SetBool("turnLeft", false);
            }
        }

        if (diver.GetBool("turnRight") == true && diver.GetBool("turnUp") == false)
        {
            if (startPos.x < rightPos.x)
            {
                isMoving = true;
                timeOfStart = Time.time;
                finishPos = new Vector3(startPos.x + 4.67f, rightPos.y, rightPos.z);
            }
            else
            {
                diver.SetBool("turnRight", false);
            }
        }

        if (diver.GetBool("turnUp") == true)
        {
            if (startPos.y != upPos.y)
            {
                isMoving = true;
                timeOfStart = Time.time;
                finishPos = upPos;
                finishPos.y = 5;
            }
           
        }
    }

    private IEnumerator UpAndDown()
    {
        float _timeSinceStart = Time.time - timeOfStart;
        float _percOfCompletion = _timeSinceStart / timeToPoint;
        lerpMov = Vector3.Lerp(startPos, finishPos, _percOfCompletion);
        yield return new WaitForSeconds(0.3f);
        _percOfCompletion = _timeSinceStart / timeToPoint;
        float _lerpMov = Mathf.Lerp(transform.position.y, 3f, _percOfCompletion);
        this.transform.position = new Vector3(startPos.x, _lerpMov);

        if(_percOfCompletion >= .1f)
        {
            diver.SetBool("turnUp", false);
            isMoving = false;

        }
    }
       
}
