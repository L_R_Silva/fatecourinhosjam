﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    [SerializeField]
    private Animator diver;
    private float timer = 0f;
    [SerializeField]
    private GameObject bubbleHit;
    public static int lives = 3;
    [SerializeField]
    private GameObject mesh;
    private bool powerUp = false;
  

    private void Update()
    {
        timer += Time.deltaTime;
        if((Input.GetKeyDown("left") || Input.GetKeyDown("a")) && diver.GetBool("turnUp") == false && timer > 0.06f && this.transform.position.x > PlayerMov.staticLeftPos.x)
        {
            diver.SetBool("turnLeft", true);
            timer = 0f;
        }

        if ((Input.GetKeyDown("right") || Input.GetKeyDown("d")) && diver.GetBool("turnUp") == false && timer > 0.06f && this.transform.position.x < PlayerMov.staticRightPos.x)
        {
            diver.SetBool("turnRight", true);
            timer = 0f;
        }

        if ((Input.GetKeyDown("up") || Input.GetKeyDown("w")) && diver.GetBool("turnLeft") == false && diver.GetBool("turnRight") == false && timer > 0.06f)
        {
            diver.SetBool("turnUp", true);
            timer = 0f;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Colectable")
        {
            if (powerUp == false)
            {
                CollectSystem.collectSharedInstance.AddScore(5);
            }
            else
            {
                CollectSystem.collectSharedInstance.AddScore(10);
            }
            other.gameObject.SetActive(false);
        }

        if(other.gameObject.tag == "Obstacle" && powerUp == false)
        {
            lives = lives - 1;
            StartCoroutine("GotHit");
            
        }
    }
    private void PowerUp()
    {
        powerUp = true;
        ChunkRoll.chunkRollInstance.speed = 65f;
    }

    private IEnumerator GotHit()
    {
        GetComponent<BoxCollider>().enabled = false;
        bubbleHit.SetActive(true);
        bubbleHit.transform.position = this.transform.position;
        for (int i = 0; i < 4; i++)
        { //So BAD XD

            mesh.SetActive(false);
            yield return new WaitForSeconds(.2f);
            mesh.SetActive(true);
            yield return new WaitForSeconds(.2f);
        }
        GetComponent<BoxCollider>().enabled = true;
        bubbleHit.SetActive(false);

    }

   
}
