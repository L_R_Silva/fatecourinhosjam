﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishFloking : MonoBehaviour {

    [SerializeField]
    private float speed;
    
	void Update ()
    {
        transform.Translate(0f, Time.deltaTime * speed, 0f);
	}
}
