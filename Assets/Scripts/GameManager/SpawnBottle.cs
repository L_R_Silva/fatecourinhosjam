﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBottle : MonoBehaviour, SpawnSystem {

    private Vector3 center;
    [SerializeField] [Range(10, 20)]
    private int maxBottle;
    [SerializeField]
    private Vector3 size;
    private List<GameObject> bottles;
    private bool isApplicationQuitting = false;
    [SerializeField]
    private Transform parent;

    public void OnDrawGizmos()
    {
        Gizmos.color = new Color(1f, 0, 0, 0.5f);
        Gizmos.DrawCube(center,size);
    }

    public void SpawnObject()
    {
        int _numBottle = Random.Range(10,maxBottle);
        int i = 0;
        while (i < _numBottle)
        {
            Vector3 _pos = center + new Vector3(0f, 2f, Random.Range(-size.z / 2, size.z / 2));
            GameObject _bottle = GarrafaPoller.garrafaSharedInstance.Pool();
            if (_bottle != null)
            {
                _bottle.transform.position = _pos;
                _bottle.GetComponent<Animator>().playbackTime = Random.Range(0f, 0.7f);
                _bottle.SetActive(true);
                _bottle.transform.SetParent(parent);
                bottles.Add(_bottle);

            }
            i++;
        }
    }

    private void Start ()
    {
        bottles = new List<GameObject>();
        center = this.gameObject.transform.position;
        SpawnObject();
    }

    private void OnDisable()
    {
        if (isApplicationQuitting) return;
        
        if(bottles != null)
        {
            for(int i = 0; i< bottles.Count; i++)
            {
                bottles[i].SetActive(false);
                bottles[i].transform.position = Vector3.zero;
            }
        }

    }
      
    private void OnEnable()
    {
        center = this.gameObject.transform.position;

        if (bottles != null)
            for (int i = 0; i < bottles.Count; i++)
            {
                Vector3 _pos = center + new Vector3(0f, 2f, Random.Range(-size.z / 2, size.z / 2));
                bottles[i].transform.position = _pos;
                bottles[i].SetActive(true);
            }
    }

    private void OnApplicationQuit()
    {
        isApplicationQuitting = true;
    }

    
}
