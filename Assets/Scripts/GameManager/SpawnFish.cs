﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFish : MonoBehaviour, SpawnSystem {

    private Vector3 center;
    [SerializeField]
    private Vector3 size;
    [SerializeField]
    private int fishLimit;
    private List<GameObject> fishes;
    private bool isApplicationQuitting = false;
    [SerializeField]
    private Transform parent;
    

    public void OnDrawGizmos()
    {
        Gizmos.color = new Color(0,1f,0,.5f);
        Gizmos.DrawCube(center, size);
    }

    public void SpawnObject()
    {
        int _numFish = Random.Range(10, fishLimit);
        int i = 0;
        while(i < _numFish)
        {
            Vector3 _pos = center + new Vector3(Random.Range(-size.x / 2, size.x / 2), Random.Range(-size.y / 2, size.y / 2), Random.Range(-size.z / 2, size.z / 2));
            GameObject _fish = FishPoller.fishSharedIntance.Pool();
            if (_fish != null)
            {
                _fish.transform.position = _pos;
                _fish.SetActive(true);
                _fish.transform.SetParent(parent);
                fishes.Add(_fish);
            }
            i++;            
        }
        
    }
	
	private void Start ()
    {
        this.gameObject.transform.localPosition = new Vector3(Random.Range(-7f,7f), 5f, Random.Range(-7f,7f));
        fishes = new List<GameObject>();
        center = this.gameObject.transform.position;
        SpawnObject();
    }

    private void OnEnable()
    {
        center = this.gameObject.transform.position;
        if (fishes != null)
            for (int i = 0; i< fishes.Count; i++)
            {
                Vector3 _pos = center + new Vector3(Random.Range(-size.x / 2, size.x / 2), Random.Range(-size.y / 2, size.y / 2), Random.Range(-size.z / 2, size.z / 2));
                fishes[i].transform.position = _pos;
                fishes[i].SetActive(true);
            }
       
    }

    private void OnDisable()
    {
        if (isApplicationQuitting) return;

        if (fishes != null)
        { 
            for (int i = 0; i< fishes.Count; i++)
            {
                fishes[i].SetActive(false);
            }
        }
    }

    private void OnApplicationQuit()
    {
        isApplicationQuitting = true;
    }
}
