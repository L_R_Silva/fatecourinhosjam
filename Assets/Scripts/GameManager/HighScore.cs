﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Score
{
    string inittials;
    int points;

    public Score(string _name, int _points)
    {
        inittials = _name;
        points = _points;
    }
}

public class HighScore : MonoBehaviour {

    public static Score[] ScoreEntrys;
    
    private void Start()
    {
        ScoreEntrys = new Score[5]; 
    }
}
