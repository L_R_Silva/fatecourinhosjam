﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChunkRoll : MonoBehaviour {

    public static ChunkRoll chunkRollInstance;
    public float speed = 40f;
    
    private void Awake()
    {
        chunkRollInstance = this;
       
    }

    private void Start()
    {
        speed = 40f;
    }

    private void Roll()
    {
        SpeedOverTime();
        this.transform.Translate(-Vector3.forward * Time.deltaTime * speed);
    }

    private void SpeedOverTime()
    {
        float _elapse = Time.time;
        if (_elapse >= 10 && speed < 45)
        {
            speed = 45f;
           
        }
        else if(_elapse >= 30 && speed < 55 )
        {
            speed = 55;
        }
        else if(_elapse >= 50 && speed < 65)
        {
            speed = 65;
        }
    }

    private void FixedUpdate()
    {
        Roll();
        
    }
}
