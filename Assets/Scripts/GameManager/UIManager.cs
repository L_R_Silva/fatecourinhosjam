﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class UIManager : MonoBehaviour {

    [SerializeField]
    private Text time;
    [SerializeField]
    private Text uiScore;
    [SerializeField]
    private Image[] powerUp;
    [SerializeField]
    private Animator fade;
    private int timeInMin = 0;
    private float timeInSec = 0;

    private void Start()
    {
        fade.SetBool("Fade",true);
    }

    private void Update()
    {
        timeInSec += Time.deltaTime;
        if (timeInSec >= 60)
        { 
            timeInMin++;
            timeInSec = timeInSec - 60;
            time.text = (timeInMin+ ":"+ Mathf.Round(timeInSec));
        }
        else
        {
            time.text = (timeInMin + ":" + Mathf.Round(timeInSec));
            uiScore.text = "X " + CollectSystem.collectSharedInstance.GetScore().ToString(); ;
        }
    }

}
