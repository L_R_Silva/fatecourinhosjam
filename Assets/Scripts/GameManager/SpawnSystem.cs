﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface SpawnSystem {

    void SpawnObject();
    void OnDrawGizmos();
   
}
