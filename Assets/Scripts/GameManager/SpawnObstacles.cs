﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObstacles : MonoBehaviour {

    [SerializeField]
    private GameObject[] SpawnPoints;
    [SerializeField]
    private int maxObs;
    private List<GameObject> obstacles;
    private bool isApplicationQuitting = false;
    [SerializeField]
    private Transform parent;

    private void SpawnObstacle()
    {
        int _numObs = Random.Range(0, maxObs);
        int i = 0;
        while(i< _numObs)
        {
            GameObject _Obs = ObstaclesPoller.obstaclesSharedInstance.Pool();
            if(_Obs != null)
            {
                _Obs.transform.position = SpawnPoints[Random.Range(0, SpawnPoints.Length)].transform.position;
                _Obs.SetActive(true);
                _Obs.transform.SetParent(parent);
                obstacles.Add(_Obs);
            }
            i++;
        }
    }

    private void Start()
    {
        obstacles = new List<GameObject>(); ;
        SpawnObstacle();
        
    }

    private void OnEnable()
    {
        if (obstacles != null)
            for (int i = 0; i < obstacles.Count; i++)
            {
                Vector3 _pos = SpawnPoints[Random.Range(0, SpawnPoints.Length)].transform.position;
                obstacles[i].transform.position = _pos;
                obstacles[i].SetActive(true);
            }
    }

    private void OnDisable()
    {
        if (isApplicationQuitting) return;
        if(obstacles != null)
            for (int i = 0; i < obstacles.Count; i++)
            {
                
                obstacles[i].SetActive(false);
            }
    }

    private void OnApplicationQuit()
    {
        isApplicationQuitting = true;    
    }
}
