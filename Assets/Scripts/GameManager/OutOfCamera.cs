﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutOfCamera : MonoBehaviour {

    private Vector3 position;

    private void OnBecameInvisible()
    {
        MapSequencer.numberOfActive -= 1;
        this.gameObject.SetActive(false);
        
    }

    private void Update()
    {
        position = Camera.main.WorldToViewportPoint(transform.position);
        if (position.z < -50 )
        {
            OnBecameInvisible();
        }
    }
}
