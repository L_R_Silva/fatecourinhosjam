﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    [SerializeField]
    private Canvas gameOverCanvas;
    [SerializeField]
    private Text scoreText;
    [SerializeField]
    private Animator gameOver;
    [SerializeField]
    private InputField nickname;

    private void Update()
    {
        if (PlayerController.lives <= 0)
        {
            StartCoroutine("ShowGameOver");
            StopGame();
        }
    }

    private void StopGame()
    {
        Time.timeScale = 0f;
    }

    private IEnumerator ShowGameOver()
    {
        gameOverCanvas.gameObject.SetActive(true);
        gameOver.SetBool("ShowGameOverMenu", true);
        scoreText.text = "Score: "+CollectSystem.collectSharedInstance.GetScore().ToString();
        if (!PlayerPrefs.HasKey("Score1"))
        {
            yield return new WaitForSecondsRealtime(.1f);
            gameOver.SetBool("NewHighScore",true);
        }
        if(CollectSystem.collectSharedInstance.GetScore() > PlayerPrefs.GetInt("Score1"))
        {
            yield return new WaitForSecondsRealtime(.1f);
            gameOver.SetBool("NewHighScore", true);
        }
        //yield return null;
    }

    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        gameOverCanvas.gameObject.SetActive(false);
        Time.timeScale = 1f;
        PlayerController.lives = 3;
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MenuPrincipal");
    }

    public void SetNewScore()
    {
        string _name = nickname.text;
        int _score = CollectSystem.collectSharedInstance.GetScore();
        PlayerPrefs.SetString("NickName1", _name);
        PlayerPrefs.SetInt("Score1",_score);
        Debug.Log(PlayerPrefs.GetInt("Score1"));
    }

    
}
