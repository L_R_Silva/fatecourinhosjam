﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSequencer : MonoBehaviour {

    private int maxNumberOfChunks = 7;
    public static int numberOfActive;
    private float chunkLenght = 40f;
    GameObject lastChunk = null;

    private void PlaceChunks(int _i = 0)
    {
        while (_i<maxNumberOfChunks)
        {
            GameObject _chunk = ChunkPoller.ChunkPollerSharedIntance.Poll();
            if(_chunk != null && _i == 0)
            {
                _chunk.transform.position = new Vector3(_chunk.transform.position.x, _chunk.transform.position.y, 0f + chunkLenght/2f);
                _chunk.SetActive(true);
                lastChunk = _chunk;
                numberOfActive += 1;
            }
            else if(_chunk != null)
            {
                _chunk.transform.position = new Vector3(_chunk.transform.position.x, _chunk.transform.position.y, lastChunk.transform.position.z + chunkLenght);
                _chunk.SetActive(true);
                lastChunk = _chunk;
                numberOfActive += 1;
            }
            _i++;
        }
    }
    private void Start()
    {
        numberOfActive = 0;
    }
    private void Update()
   {
        if (numberOfActive < maxNumberOfChunks)
        {
            PlaceChunks(numberOfActive);
        }
    }

    

}
