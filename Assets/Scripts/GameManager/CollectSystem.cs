﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectSystem : MonoBehaviour {

    public static CollectSystem collectSharedInstance;
	private int Score = 0;

    private void Awake()
    {
        collectSharedInstance = this;
    }
    public void AddScore(int _points)
    {
        Score += _points;
       
    }

    public int GetScore()
    {
       // Debug.Log(Score);
        return Score;
    }

    

}
