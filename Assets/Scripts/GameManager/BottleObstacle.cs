﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottleObstacle : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Obstacle")
        {
            this.gameObject.SetActive(false);
        }
    }

}
