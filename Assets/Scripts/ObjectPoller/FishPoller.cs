﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishPoller : MonoBehaviour {

    public static FishPoller fishSharedIntance;
    [SerializeField]
    private GameObject[] fish;
    List<GameObject> fishes;

    private void Awake()
    {
        fishSharedIntance = this;
        fishes = new List<GameObject>();
        for (int i = 0; i < 50; i++)
        {
            for (int j = 0; j < fish.Length; j++)
            {
                GameObject _temp = (GameObject)Instantiate(fish[j]);
                _temp.SetActive(false);
                fishes.Add(_temp);
            }
        }
    }

    private void Start()
    {
        
    }
    public GameObject Pool()
    {
        //int b = fishes.Count;
        for (int i = 0; i < fishes.Count; i++)
        {
            if(!fishes[i].activeInHierarchy && fishes[i].transform.parent == null)
                return fishes[i];
            
        }
        return null;
    }
}
