﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesPoller : MonoBehaviour {

    public static ObstaclesPoller obstaclesSharedInstance;
    [SerializeField]
    private GameObject obstacle;
    private List<GameObject> obstacles;

    private void Awake()
    {
        obstaclesSharedInstance = this;
        obstacles = new List<GameObject>();
        for (int i = 0; i < 24; i++)
        {
            GameObject _temp = (GameObject)Instantiate(obstacle);
            _temp.SetActive(false);
            obstacles.Add(_temp);
        }
    }

    public GameObject Pool()
    {
        for (int i = 0; i < obstacles.Count; i++)
        {
            if (!obstacles[i].activeInHierarchy && obstacles[i].transform.parent == null)
                return obstacles[i];
        }
        return null;
    }
}
