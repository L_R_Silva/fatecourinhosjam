﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChunkPoller : MonoBehaviour {

    public static ChunkPoller ChunkPollerSharedIntance;
    [SerializeField]
    private GameObject[] GOChunks;
    private List<GameObject> chunks;
    private System.Random rnd = new System.Random();

    private void Awake()
    {
        ChunkPollerSharedIntance = this;
        chunks = new List<GameObject>();
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < GOChunks.Length; j++)
            {
                GameObject _temp = (GameObject)Instantiate(GOChunks[j]);
                _temp.SetActive(true);
               chunks.Add(_temp);
            }
        }
    }

    private void Start()
    {
        for (int i = 0; i < chunks.Count; i++)
        {
            chunks[i].SetActive(false);
        }

    }
    public GameObject Poll()
    {
        for (int i = 0; i < chunks.Count; i++)
        {
            Shuffle(chunks);// PIOR CASO -_-
            if (!chunks[i].activeInHierarchy)
            {
                return chunks[i];
            }
        }
        return null;
    }

    private void Shuffle(List<GameObject> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rnd.Next(n + 1);
            GameObject _temp = list[k];
            list[k] = list[n];
            list[n] = _temp;
        }
    }

}
