﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GarrafaPoller : MonoBehaviour {

    public static GarrafaPoller garrafaSharedInstance;
    [SerializeField]
    private GameObject garrafa;
    private List<GameObject> garrafas;

    private void Awake()
    {
        garrafaSharedInstance = this;
        garrafas = new List<GameObject>();
        for (int i = 0; i < 320; i++)
        {
            GameObject _temp = (GameObject)Instantiate(garrafa);
            _temp.SetActive(false);
            garrafas.Add(_temp);
        }
    }
      
    public GameObject Pool()
    {
        for (int i = 0; i < garrafas.Count; i++)
        {
            if (!garrafas[i].activeInHierarchy && garrafas[i].transform.parent == null)
                return garrafas[i];
        }
        return null;
    }


}

